import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {TournamentsPage} from "../tournaments/tournaments";
import {DbApiService} from "../../shared/db-api.service";
import {TeamHomePage} from "../team-home/team-home";
import {UserSettings} from "../../shared/UserSettings";

/**
 * Generated class for the MyTeamsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-teams',
  templateUrl: 'my-teams.html',
})
export class MyTeamsPage {

  favourites = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loaderContorller: LoadingController,
              public dbapi: DbApiService,
              public userSettings: UserSettings) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyTeamsPage');
  }

  ionViewWillEnter() {
    this.userSettings.getAllFavourites().then( value => this.favourites = value);
    console.log('ionViewWillEnter MyTeamsPage', this.favourites);
  }

  irATorneos() {
    this.navCtrl.push(TournamentsPage);
  }

  favouriteTapped($event, item) {
    let loader = this.loaderContorller.create({
      content: "Accediendo a los datos",
      spinner: "dots",
      dismissOnPageChange: true
    });
    loader.present();

    this.dbapi.getTournamentData(item.tournamentId)
      .subscribe( data =>this.navCtrl.push(TeamHomePage, item.team));
  }
}
