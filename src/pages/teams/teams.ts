import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {TeamHomePage} from "../team-home/team-home";
import {DbApiService} from "../../shared/db-api.service";
import * as _ from 'lodash'

/**
 * Generated class for the TeamsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teams',
  templateUrl: 'teams.html',
})
export class TeamsPage {

  teams: any = [];
  selectedTourney: any;
  private allTeams: any;
  private allTemaDivision: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dbap: DbApiService,
              public loaderController: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamsPage');
    console.log('ionViewDidLoad TeamsPage data:', this.navParams.data);
    this.selectedTourney = this.navParams.data;

    let loader = this.loaderController.create({
      content: 'Accediendo a los datos',
      spinner: 'dots'
    });

    loader.present().then(() => {
      this.dbap.getTournamentData(this.selectedTourney.id)
        .subscribe(data => {
          this.allTeams = this.teams;
          this.allTemaDivision = _.chain(data.teams)
            .groupBy('division')
            .toPairs()
            .map(item =>
              _.zipObject(['divisionName', 'divisionTeams'], item))
            .value();
          this.teams = this.allTemaDivision;
          loader.dismiss();
          console.log("allTeamDivision", this.allTemaDivision);
        });
    });

  }

  teamTapped($event: MouseEvent, item: { name:string }) {
    this.navCtrl.push(TeamHomePage, item);
  }
}
