import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import * as _ from 'lodash';
import {DbApiService} from "../../shared/db-api.service";
import {TeamHomePage} from "../team-home/team-home";
import * as moment from 'moment';
import {MomentInput} from 'moment';
import {UserSettings} from "../../shared/UserSettings";

/**
 * Generated class for the TeamDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-team-detail',
  templateUrl: 'team-detail.html',
})
export class TeamDetailPage {

  team : any = {};
  games: any = [];
  tourneyData : any;
  teamStanding: any = {};
  dateFilter: string;
  allGames  : any[];
  useDateFilter: boolean = false;
  isFollowing  : boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dbapi:DbApiService,
              public toastcontroller: ToastController,
              public userSettings:UserSettings) {
    console.log("team detail", this.navParams);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad TeamDetailPage');
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter TeamDetailPage');

    this.team = this.navParams.data;
    this.tourneyData = this.dbapi.getCurrentTourney();

    this.games = _.chain(this.tourneyData.games)
      .filter(g => g.team1Id === this.team.id || g.team2Id === this.team.id)
      .map(g => {
        let isTeam1 = (g.team1Id === this.team.id);
        let opponentName = isTeam1 ? g.team2 : g.team1;
        let scoreDisplay = this.getScoreDisplay(isTeam1, g.team1Score, g.team2Score);
        return {
          gameId: g.id,
          opponent: opponentName,
          time: Date.parse(g.time),
          location: g.location,
          locationUrl: g.locationUrl,
          scoreDisplay: scoreDisplay,
          homeAway: (isTeam1 ? "vs." : "at")
        };
      })
      .value();
    this.allGames = this.games;
    this.teamStanding = _.find(this.tourneyData.standings, {'teamId': this.team.id});
    this.userSettings.isFavouriteTeam(this.team.id).then(value => this.isFollowing= value);
    console.log("partidos: ",this.games);
  }

  getScoreDisplay(isTeam1, team1Score, team2Score) {
    if (team1Score && team2Score) {
      let teamScore = (isTeam1 ? team1Score : team2Score);
      let opponentScore = (isTeam1 ? team2Score : team1Score);
      let winIndicator = teamScore > opponentScore ? "W: " : "L: ";
      return winIndicator + teamScore + "-" + opponentScore;
    }
    else {
      return "";
    }
  }

  gameClicked($event, game: any) {
    let opponent = this.tourneyData.teams.find(g => g.name == game.opponent);
    this.navCtrl.parent.parent.push(TeamHomePage, opponent);
  }

  dateChanged() {
    if (this.useDateFilter) {
      this.games = _.filter(this.allGames, g =>
        moment(g.time).isSameOrAfter(this.dateFilter as MomentInput, 'day')
      )
    } else {
      this.games = this.allGames;
    }
  }

  toggleFollow() {
    this.isFollowing = !this.isFollowing;
    if (this.isFollowing)
      this.userSettings.favouriteTeam(this.team,
        this.tourneyData.tournament.id,
        this.tourneyData.tournament.name);
    else
      this.userSettings.unFavouriteTeam(this.team);

    let toast = this.toastcontroller.create({
      message: `${this.isFollowing ? `sigues a ${this.team.name}` : 'ya no sigues a este equipo'}`,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
