import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {TeamsPage} from "../teams/teams";
import {DbApiService} from "../../shared/db-api.service";

/**
 * Generated class for the TournamentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tournaments',
  templateUrl: 'tournaments.html',
})
export class TournamentsPage {

  tournaments: any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dbapi: DbApiService,
              public loadingController: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TournamentsPage');

    let loader = this.loadingController.create({
      content: 'Accediendo a los datos',
      spinner: 'dots'
    });

    loader.present().then(() => {
      this.dbapi.getTournaments()
        .subscribe(data => this.tournaments = data);
      loader.dismiss();

    });

  }

  ionViewWillLoad() {
    console.log('ionViewWillLoad TournamentsPage');
  }

  goHome() {
    this.navCtrl.pop();
  }

  itemTapped(item) {
    this.navCtrl.push(TeamsPage, item);
  }
}
