import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "@angular/fire/database";
import {Observable} from "rxjs/Observable";
import {map} from "rxjs/operators";

@Injectable()
export class DbApiService {
  currentTourney: any = {};

  constructor(private fdb: AngularFireDatabase) {
  }

  getTournaments(): Observable<any> {
    return this.fdb.list('/tournaments').valueChanges();
  }

  getTournamentData(tourneyId):Observable<any> {

    return this.fdb.object(`tournaments-data/${tourneyId}`).valueChanges()
      .pipe(
          map(resp => this.currentTourney = resp)
      );
  }

  getCurrentTourney() {
    return this.currentTourney;
  }
}
