import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage"

@Injectable()
export class UserSettings {

  favouries = [];

  constructor(private storage: Storage){};


  favouriteTeam(team, tournamentId, tournamentName) {
    let item={
      team: team,
      tournamentId: tournamentId,
      tournamentName: tournamentName
    };
    this.storage.set(team.id.toString(), JSON.stringify(item));
  }

  unFavouriteTeam(team) {
    this.storage.remove(team.id.toString());
  }

  isFavouriteTeam(teamId) {
    return this.storage.get(teamId.toString()).then(value =>
    value ? true : false)
  }

  async getAllFavourites() {
    let item = [];
    await this.storage.forEach((value, key, index) => {
      item.push(JSON.parse(value))
    });
    this.favouries = item;
    return item;
  }
}
