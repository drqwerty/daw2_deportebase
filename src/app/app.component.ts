import { Component, ViewChild } from '@angular/core';
import {LoadingController, Nav, NavController, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {MyTeamsPage} from "../pages/my-teams/my-teams";
import {TournamentsPage} from "../pages/tournaments/tournaments";
import {DbApiService} from "../shared/db-api.service";
import {UserSettings} from "../shared/UserSettings";
import {TeamHomePage} from "../pages/team-home/team-home";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = MyTeamsPage;
  pages: Array<{title: string, component: any}>;
  private hideGroup = true;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public userSettings: UserSettings,
              public loaderContorller: LoadingController,
              public dbapi: DbApiService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  goHome() {
    this.nav.setRoot(MyTeamsPage);
  }

  goTournaments() {
    this.nav.push(TournamentsPage);
  }

  favouriteTapped($event, item) {
    let loader = this.loaderContorller.create({
      content: "Accediendo a los datos",
      spinner: "dots",
      dismissOnPageChange: true
    });
    loader.present();


    this.dbapi.getTournamentData(item.tournamentId)
      .subscribe( data => this.nav.push(TeamHomePage, item.team));
  }

  toggleGroup() {
    this.hideGroup = !this.hideGroup;
  }
}
